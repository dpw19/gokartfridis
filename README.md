a# goKartFridis

¿Podemos hacer, esto? pregunta Francis. Sí, creo que es posible, podemos hacerlo
como un proyecto colaborativo y libre.

Esta es la documentación de dicho proceso.

## Conformación del grupo y primeros prototipos

En la primera sesión se trabajó con cartón, lápiz y papel para generar las
primeras ideas de gokart. Surgieron tres propuestas, de la cuales la más viable
fue la del prototipo 1.


![Prototipo 1](img/01prototipo1.png =200x200)

![Prototipo 2](img/02prototipo2.png =200x200)

![Prototipo 1](img/03prototipo3.png =200x200)

## La escala y los cortes

Se adquirió una hoja de triplay de 15mm (aunque es recomendable usar de 19mm)
con donde se cortaron las siguientes piezas

![cortes](img/04cortes.png =200x200)

## Base del volante

Para la base del volante experimentamos hacer los cortes con router CNC.
Los trazos se realizaron en [Inkscape](https://inkscape.org) para obtener un 
archivo en formato SVG, mismo que se envío a la maderería que proporciona
este servicio.

Los archivos svg se encuentran en la carpeta [vectoriales](vectoriales).

## Presupuesto

### Tornillería, baleros, llantas
| **Cantidad** | **Descripción** | **Costo [MXN]** |
| ------------ | --------------- | --------- |
| 1 | Varilla Roscada 1/2" | 70 |
| 2 | Tuercas |  |
| 2 | Rondana Plana |  |
| 2 | Rondana Presión | 20 |
| 2 | Tornillo hexagonal 3/8x3" | 5 |
| 2 | Tuerca standar 1/2 | 3 |
| 0.1 | kg Rondana 7/16  | 5.5 |
| 1 | abrazadera 1/2" omega | 4 |
| 1 | Abrazadera Sin fin 06 | 6 |

### Consumibles
| **Cantidad** | **Descripción** | **Costo [MXN]** |
| ------------ | --------------- | --------- |
| 2 | Lija Esmeril | 30 |


### Motor
| **Cantidad** | **Descripción** | **Costo [MXN]** |
| ------------ | --------------- | --------- |
| 1 | Motor limpiadores | 450 |


### Herramientas
 
| **Cantidad** | **Descripción** | **Costo** |
| ------------ | --------------- | --------- |
| 1 | Juego Brocas Planas | 169 |



## Licencia



## Deslinde de responsabilidad
